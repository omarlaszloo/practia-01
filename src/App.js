import React, { useState, useEffect } from 'react'
import './App.css';
import Header from './conatiners/Header'
import CardInfo from './conatiners/AvailableBalance'
import TableConfig from './conatiners/TableConfig'
import HomeMenu from './conatiners/HomeMenu'
import data from './utils/data/index.json'

function App () {
	
	const [info, setInfo] = useState()

	const [cardPayment, setCardPayment] = useState()

	useEffect(() => {
		setInfo(data[0])
	}, [])

	const item = info !== undefined ? info.response : ''
	const {
		card1,
		card2,
		menu,
		movimientos,
	} = item

	const itemPayment = (params) => {
		console.log(params)
		setCardPayment(params)
	}

	const cardDeleteItem = (itemParams) => {
		const { movimientos } = info.response
		const item = movimientos.find(element => element.data.id === itemParams.id)
		
		const index = movimientos.indexOf(item);
		
		if (index > -1) {
			movimientos.splice(index, 1);
		}

		setCardPayment(info.response.movimientos)

	}

	return (
		<div className="App">
			<div className="container">
				<Header name="Pepito"/>
				<CardInfo {...card1} />
			</div>
			<div>
				<HomeMenu menu={menu} />
				<CardInfo {...cardPayment} {...card1} cardDeleteItem={cardDeleteItem} />
			</div>
			<div>
				<TableConfig movimientos={movimientos} itemPayment={itemPayment} />
			</div>
		</div>
	)
}

export default App;
