import React from 'react'
import './index.css'

const Avaible = ({ price, title, data, config, costo, cardDeleteItem }) => {
    console.log('costo', data)
    return (
        <React.Fragment>
            <div className={`${data ? data.config : config} row-avaible`}>
                <p className="container-avaible-title">{title}</p>
                <p className="container-avaible-price">{price}</p>
            
            { data ?
                <div className="section-info">
                    <div className="section-payment">
                        <div className="row">
                            <p>Fecha de pago</p>
                            <strong>{data.fecha_pago}</strong>
                        </div>
                        <div className="row">
                            <button
                                onClick={() => cardDeleteItem(data)}
                                className="btn-payment">pagar ahora</button>
                        </div> 
                    </div>
                    <div className="section-payment">
                        <div className="row">
                            <p>Mis logros</p>
                        </div>
                        <div className="row row-price">
                            {data.mis_logros}
                        </div>
                    </div>

                    <div className="section-payment">
                        <div className="row">
                            <p>Compras con TDC</p>
                        </div>
                        <div className="row row-price">
                            {data.compras_tdc}
                        </div>
                    </div>
                    
                </div>
            : ''}
            </div>
        </React.Fragment>

    )
}

export default Avaible