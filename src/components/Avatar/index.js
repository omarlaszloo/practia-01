import React from 'react'
import './index.css'

const Avatar = ({ img }) => {
    return (
        <img
            src={img}
            className="avatar"
            alt=""
        />
    )
}

export default Avatar