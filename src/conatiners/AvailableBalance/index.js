import React from 'react'
import Avaible from '../../components/Available'
import "./index.css"


const AvailableBalance = ({
    price,
    config,
    data,
    costo,
    cardDeleteItem,
}) => {
    console.log('costo', costo)
    return (
        <div className="container-available-balance row-balance">
            <Avaible
                price={price}
                config={config}
                title="Saldo disponible"
                cardDeleteItem={cardDeleteItem}
                data={data ? data : ''}
            />
        </div>
    )
}

export default AvailableBalance