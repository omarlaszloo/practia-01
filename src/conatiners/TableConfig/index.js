import React from 'react'
import './index.css'


const TableConfig = ({ movimientos, itemPayment }) => {
    const items = movimientos !== undefined ? movimientos : []
    return(
        <React.Fragment>
            <h3 className="section-title">Tus movimientos</h3>
            <table>
                {items.map(item => {
                    return(
                        <tr
                            onClick={() => itemPayment(item)}>
                            <td><img src={item.companyIcon} /></td>
                            <td className="td-description"><strong>{item.companyTitle}</strong><br />{item.description}</td>
                            <td className="td-laste-activity">{item.time}<br />{item.costo}</td>
                        </tr>
                    )
                })}
            </table>
        </React.Fragment>
    )
}

export default TableConfig