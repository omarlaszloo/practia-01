import React from 'react'
import Avatar from '../../components/Avatar'
import avatar from '../../assets/img/avatar.png'
import './index.css'


const Header = ({ name }) => {
    const text = `Hola ${name}`
    return(
        <div className="container-header">
            <div className="row-header">
                <div className="row-header-title">
                    {text}
                </div>
                <div>
                    <Avatar img={avatar} />
                </div>
            </div>
        </div>
    )
}

export default Header