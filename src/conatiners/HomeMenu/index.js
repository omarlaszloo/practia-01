import React from 'react'
import "./index.css"

const HomeMenu = ({ menu }) => {
    const items =  menu !== undefined ? menu : []
    return(
        <div className="container-home-menu">
            <ul>
                {
                    items.map(item => {
                        return(
                            <li>
                                {item.text}
                            </li>
                        )
                    })
                }
            </ul>
        </div>
    )
}

export default HomeMenu